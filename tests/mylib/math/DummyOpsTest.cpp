#include "mylib/math/DummyOps.h"

/**
 * @brief Main function for testing DummyOps functionality.
 * @return 0 indicating successful execution.
 */
int main() {
    // Write your tests for DummyOps here

    MyLib::DummyOps objB;
    objB.Add(1, 1); // Example of calling the Add method
    
    return 0;
}