#include "mylib/Foo.h"

/**
 * @brief Main function for testing Foo functionality.
 * @return 0 indicating successful execution.
 */
int main() {
    // Write your tests for Foo here

    MyLib::Foo objA;
    objA.Subtract(1, 1); // Example of calling the Subtract method
    
    return 0;
}