#include <iostream>
#include "mylib/Foo.h"

/**
 * @brief Main function for conducting a stress test.
 * @return The result of the stress test comparison.
 */
int main() {
    // Write your stress test here

    MyLib::Foo john;
    
    std::string say = john.SayHi("Jane");

    std::cout << say << std::endl;
    std::cout << "Stress test completed!" << std::endl;

    return say.compare("Hi Jane, I'm Foo!");
}