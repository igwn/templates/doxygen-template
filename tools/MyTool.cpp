/**
 * @file main.cpp
 * @brief A simple C++ program printing "Hello World!" to the console.
 */

#include <iostream>

/**
 * @brief The main function, the entry point of the program.
 * @return 0 on successful execution.
 */
int main() {
    // Prints "Hello World!" to the console
    std::cout << "Hello World !" << std::endl;
    
    return 0; // Returning 0 to indicate successful execution
}