#ifndef MyLib_DummyOps_H
#define MyLib_DummyOps_H

#include "mylib/math/InternalHelpers.h"

namespace MyLib {

    /**
     * @class DummyOps
     * @brief Represents a class providing basic arithmetic operations within the MyLib namespace.
     */
    class DummyOps {

    public:
    
        int Add(int a, int b);
        int Subtract(int a, int b);
        int Multiply(int a, int b);
        double Divide(int a, int b);
    };
};

#endif // DummyOps_H