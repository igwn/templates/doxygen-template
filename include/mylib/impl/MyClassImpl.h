#pragma once

#include "MyClass.h"

template <typename Element>
class MyClass<Element>::Impl {

    friend class MyClass<Element>;

protected:
    Impl() {}

public:
    virtual ~Impl() {}

    void Print(const std::vector<int> &indices);
};
