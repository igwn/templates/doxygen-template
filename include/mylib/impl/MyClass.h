#pragma once

#include <iostream>
#include <vector>

template <typename Element>
class MyClass {

    private:
        class Impl;
        Impl *pImpl;

    protected:
        std::vector<int> indices;

    public:

        MyClass();
        MyClass(std::vector<int> indices);
        virtual ~MyClass();

        template <typename... Indices>
        MyClass(Indices... indices): MyClass(std::vector<int>({indices...})) {}

        void Print();
};
