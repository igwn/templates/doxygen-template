#ifndef MyLib_Bar_H
#define MyLib_Bar_H

#include <iostream>
#include <string>

#include "mylib/math/DummyOps.h"

namespace MyLib {

    /**
     * @class Bar
     * @brief Represents a class derived from DummyOps within the MyLib namespace.
     */
    class Bar : public DummyOps {
    public:
    
        Bar();
        ~Bar();

        virtual std::string SayHi(const std::string& name);
        // Add any necessary methods or members for Bar
    };
}
#endif // Bar_H