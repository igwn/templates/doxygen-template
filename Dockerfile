# Use the ubuntu image as the base image
FROM ubuntu:latest

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

# Update package lists and install necessary packages
COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages) wget git && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

# Additional conda environment
RUN wget -O Miniforge3.sh "https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-$(uname)-$(uname -m).sh"
RUN sh Miniforge3.sh -b -p "/opt/conda" && rm Miniforge3.sh

ENV PATH=/opt/conda/bin:$PATH

COPY environment.yml .
RUN mamba env create --file environment.yml && \
    mamba clean --all --yes

RUN conda init
RUN ENVNAME=$(cat environment.yml | grep 'name:' | cut -d':' -f2 | sed 's/[ ]*//') && \
    echo "conda activate $ENVNAME" >> ~/.bashrc

WORKDIR /root
CMD ["/bin/bash"]

# Set any other configurations or commands as needed
# [..]
