# Doxygen C++ Library Template

This is a specialized template for library development meant for maintaining advanced projects including some automation and response doxygen documentation.

## Features

- **Library**: Compile the library using `./bootstrap.sh` or running `cmake && make -j && make install`.
- **Tests**: Run tests with `make tests`.
- **Examples**: Access executable examples (located in the build directory) illustrating core library features.
- **Tools**: Executable tools designed to complement the library functionalities, installed into the final directory.
- **Doxygen Documentation**: Generates a responsive webpage (using Doxygen Awesome) including versioning and additional documentation information based on markdown files located in `./docs`.
- **Docker Integration**: Dockerfile provided for custom image creation, facilitating CI/CD workflows.
- **GitLab CI/CD**: GitLab CI/CD templates to set up the environment, compile, and deploy library documentation through a dedicated pipeline.

## Source Code and Directory Structure

The repository's `./src` directory houses the core source code of the library, while the `./include` directory stores essential header files required for the library functionalities.

The `CMakeLists.txt` file simplifies library compilation and manages dependencies, connecting with subdirectories:
- **Tests (`./tests`)**: Hosts test suites ensuring library functionality.
- **Examples (`./examples`)**: Contains executable examples showcasing various usage scenarios of the library.
- **Tools (`./tools`)**: Stores executable tools designed to enhance and complement the library's functionalities.

## Documentation

### Generation

Generate documentation using Doxygen's `make docs` after preparing the `build/` directory. The `docs` directory contains Markdown files instrumental in generating comprehensive Doxygen documentation, available post-compilation in `./public`. Serve this directory using HTTP servers like NGINX or Apache2.

## GitLab CI/CD and Pages Integration

Seamlessly integrate the project with GitLab CI/CD for automated testing, building, and deployment. GitLab Pages are automatically generated upon successful pipeline completion and tag publication, configurable via the GitLab repository settings post the initial successful pipeline run.

The CI/CD workflow executes on a GitLab instance, compiling the library within a Docker container, conducting comprehensive tests, and deploying documentation upon creating a new tag.

## Docker Container

Utilize the provided Dockerfile for generating Docker containers, updating the Docker image as needed during CI/CD. For a new repository, execute commands like `make image` or `make image-publish` for initial image preparation. Customize the default DockerHub registry using `-DDOCKER_REGISTRY=https://<your url>` within the cmake command.

## Usage

1. **Clone the Repository**:

    ```bash
    git clone --recursive https://git.ligo.org/kagra/templates/doxygen-template doxygen-cpp-library
    cd doxygen-cpp-library
    ```

2. **Bootstrap the Project**:

    Run `./bootstrap.sh` to prepare, compile, and install the project:

    ```bash
    ./bootstrap.sh
    ```

    This script prepares the project, compiles the code, and installs necessary components. Rerun the cmake command with `./bootstrap.sh 1` to reconfigure your project.

3. **Building and Testing**:

    Execute tests to ensure library functionality:
    ```bash
    make tests
    ```

4. **Generating Documentation**:

    Generate comprehensive documentation, including versioning and additional Markdown information:

    ```bash
    make doxygen
    ```

    Access the generated documentation by opening `./public/index.html` in a web browser.

5. **Exploring Examples**:

    Explore the executable examples located in the `examples/` directory to understand and visualize various library usage scenarios.

6. **Docker Integration**:

    Utilize the provided Dockerfile to streamline CI/CD workflows.

## Contributions and Support

Contributions, bug reports, and feature requests are encouraged! Refer to CONTRIBUTING.md for detailed guidelines.
