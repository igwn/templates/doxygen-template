#ifndef MyLib_VirtualExample_H
#define MyLib_VirtualExample_H

#include <iostream>
#include "mylib/Foo.h"

/**
 * @file VirtualExample.h
 * @brief Contains declarations for Base and Derived classes demonstrating inheritance and polymorphism.
 */

// Base class
class Base
{
public:
    /**
     * @brief Virtual function to display a message from the base class.
     */
    virtual void Display() {
        std::cout << "This is the base class display function." << std::endl;
    }

    /**
     * @brief Non-virtual function to show a message from the base class.
     */
    void Show() {
        std::cout << "This is the base class show function." << std::endl;
    }
};

// Derived class overriding the virtual function
class Derived : public Base
{
public:
    /**
     * @brief Override of the base class Display function to show a message from the derived class.
     */
    void Display() override {
        std::cout << "This is the derived class display function." << std::endl;
    }
};

#endif