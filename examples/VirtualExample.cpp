/**
 * @file VirtualExample.cpp
 * @brief A demonstration of inheritance, polymorphism, and function calls in C++.
 */

#include "VirtualExample.h" // Include necessary header file(s) for class declarations

/**
 * @brief The main function demonstrates inheritance, polymorphism, and function calls.
 * @return 0 on successful execution.
 */
int main() {
    MyLib::Foo foo; // Creating an instance of MyLib::Foo

    Base *basePtr; // Declaring a pointer to Base class
    Base baseObj; // Creating an object of Base class
    Derived derivedObj; // Creating an object of Derived class

    basePtr = &baseObj; // Pointing basePtr to baseObj
    basePtr->Display(); // Calls the Display() function of the Base class

    basePtr = &derivedObj; // Pointing basePtr to derivedObj (polymorphism)
    basePtr->Display(); // Calls the Display() function of the Derived class due to polymorphic behavior

    baseObj.Show(); // Calls the Show() function of the Base class
    derivedObj.Show(); // Calls the Show() function of the Base class (not overridden in Derived class)

    return 0; // Returning 0 to indicate successful execution
}