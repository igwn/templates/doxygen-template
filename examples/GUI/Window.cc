#include <iostream>

/**
 * @file Window.cc
 * @brief Prints an ASCII art "HELLO WORLD" message to the console.
 */

/**
 * @brief The main function prints an ASCII art representation of "HELLO WORLD" to the console.
 * @return 0 on successful execution.
 */
int main() {
    // Printing the ASCII art "HELLO WORLD"
    std::cout << std::endl;
    std::cout << "\t -- HELLO WORLD -------------------------" << std::endl <<
                 "\t|\t⠀⠀⠀⠀⡠⠒⠒⠲⢤⡀⡔⢋⣑⣒⠦⢤⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⠀⠀⡠⠋⣠⣶⣿⣿⣦⡙⣿⠜⢿⣿⣿⣦⠘⡆⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⠀⢰⢃⢰⣿⣿⣿⣿⡿⣱⠏⢳⢦⣭⣽⣻⣤⠇⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⠀⢸⡀⠫⢟⣟⣛⢁⡼⠋⠉⠛⠀⠀⠈⠉⠉⠙⠉⠉⠉⠉⠓⢦⣀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⢠⢟⣿⠦⢤⡤⠖⠋⢀⣤⠴⠦⣄⠀⡤⠶⢦⡀⠀⠀⠀⠀⠀⠀⠈⠳⣄⠀⠀\t|" << std::endl << 
                 "\t|\t⠿⣞⡟⠀⢰⠃⠀⠀⠈⠁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠘⢦⠀\t|" << std::endl << 
                 "\t|\t⠀⠈⢧⠀⢸⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢠⣶⣾⣷⣶⠀⠀⠀⠀⠈⢧\t|" << std::endl << 
                 "\t|\t⠀⠀⠸⡀⢸⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠛⠛⠉⠀⠀⠀⠀⠀⢸\t|" << std::endl << 
                 "\t|\t⠀⠀⣠⠁⠈⡇⠀⠀⠀⣠⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⡀⠀⠀⠀⢸\t|" << std::endl << 
                 "\t|\t⡖⠚⠁⠀⠀⢹⡀⠀⡼⠘⢦⣄⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⢳⠀⠀⢀⠏\t|" << std::endl << 
                 "\t|\t⢷⠀⠀⠀⠀⠀⢧⡀⠹⢄⡞⠀⠙⡖⠤⠤⡤⠤⠤⢤⠒⠊⢹⣄⣜⡠⠖⠁⠀\t|" << std::endl << 
                 "\t|\t⠈⠳⢤⣀⡀⠀⠀⢳⣄⠀⠉⠙⠚⠢⠤⠴⢧⡤⠴⠚⠛⠉⠉⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⠀⠀⠀⠀⠉⠉⠉⠛⠛⠛⠒⢦⡀⠀⠀⢰⠋⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢹⠶⠿⢻⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t|\t⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣸⡕⠊⠁⠱⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀\t|" << std::endl << 
                 "\t ---------------------------------------"  << std::endl << std::endl;
    
    return 0; // Returning 0 to indicate successful execution
}