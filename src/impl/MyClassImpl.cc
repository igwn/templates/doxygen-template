#include "mylib/impl/MyClassImpl.h"

template <typename Element>
void MyClass<Element>::Impl::Print(const std::vector<int>& indices) {
    std::cout << "MyClass(" << indices.size() << " indices)" << std::endl;

    for (auto idx : indices) {
        std::cout << "Index: " << idx << std::endl;
    }
}