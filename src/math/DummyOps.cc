#include "mylib/math/DummyOps.h"

namespace MyLib {

    /**
     * @brief Adds two integers using internal helper function.
     * @param a The first integer.
     * @param b The second integer.
     * @return The result of the addition.
     */
    int DummyOps::Add(int a, int b) {
        return MyLib::Add(a, b);
    }

    /**
     * @brief Subtracts one integer from another using internal helper function.
     * @param a The first integer (minuend).
     * @param b The second integer (subtrahend).
     * @return The result of the subtraction.
     */
    int DummyOps::Subtract(int a, int b) {
        return MyLib::Sub(a, b);
    }

    /**
     * @brief Multiplies two integers using internal helper function.
     * @param a The first integer.
     * @param b The second integer.
     * @return The result of the multiplication.
     */
    int DummyOps::Multiply(int a, int b) {
        return MyLib::Mul(a, b);
    }

    /**
     * @brief Divides one integer by another using internal helper function.
     * @param a The dividend.
     * @param b The divisor.
     * @return The result of the division as a double. Returns NaN if the divisor is 0.
     */
    double DummyOps::Divide(int a, int b) {
        return MyLib::Div(a, b);
    }
}