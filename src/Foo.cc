#include "mylib/Foo.h"

namespace MyLib {

    /**
     * @brief Constructor for the Foo class.
     * Prints a message when an object of Foo is created.
     */
    Foo::Foo() {
        std::cout << "Hi I'm Foo !" << std::endl;
        #ifdef ROOT_FOUND
            std::cout << "I have heart you know about ROOT !" << std::endl;;
        #endif
    }

    /**
     * @brief Destructor for the Foo class.
     * Prints a farewell message when an object of Foo is destroyed.
     */
    Foo::~Foo() {
        std::cout << "Goodbye! I am Foo and I'm leaving now." << std::endl;
    }

    /**
     * @brief Greets a specified name.
     * @param name The name to greet.
     * @return A greeting message including the name.
     */
    std::string Foo::SayHi(const std::string& name) {
        return std::string("Hi " + name + ", I'm Foo!");
    }
}